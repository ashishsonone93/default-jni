//
// Created by ashish on 11/15/16.
//
#include <string>
#include <cstdio> //popen, fgets, etc
#include <vector>
#include <sstream>
#include <locale>

using namespace std;

#ifndef DEFAULTJNI_NATIVE_LIB_H
#define DEFAULTJNI_NATIVE_LIB_H

void split(const string &s, char delim, vector<string> &elems) {
    stringstream ss;
    ss.str(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
}


vector<std::string> split(const string &s, char delim) {
    vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}


std::string trim(const string& str,
                 const string& whitespace = " \t\n")
{
    long strBegin = str.find_first_not_of(whitespace);
    if (strBegin == string::npos)
        return ""; // no content

    long strEnd = str.find_last_not_of(whitespace);
    long strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

string toLowercase(const string x){
    std:locale loc;
    string lower;
    for(string::size_type i=0; i<x.size(); i++){
        lower += tolower(x[i], loc);
    }
    return lower;
}

#endif //DEFAULTJNI_NATIVE_LIB_H
