#include <jni.h>
#include <android/log.h>

#include <string>
#include <cstdio> //popen, fgets, etc
#include <vector>

using namespace std;

#define  LOG_TAG    "__RootFlame"
#define  LOGD(...)  if (1) __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__);

#include "native-lib.h"
#include "ed.h"

/* - - - - - helper start - - - - - -*/

/*
 * Execute a given command and return its output as string
 */
string exec(const string com){
    const char * command = com.c_str();
    LOGD("executing command : %s", command);
    FILE *in;
    char buff[512];
    string output;

    if(!(in = popen(command, "r"))){
        return "<error>";
    }

    while(fgets(buff, sizeof(buff), in)!=NULL){
        output += buff;
    }
    pclose(in);
    return output;
}

/*
 * get paths stored in $PATH enviroment variable
 */
string getPathCommand(){
    return dec("XkcPa3I4RXsUQVYUV1F8OR==");
}
vector<string> getPaths(){
    string result = exec(getPathCommand());
    return split(result, ':');
}

/*
 * Return files
 * owned by root:root,
 * not containing shell
 * but publicly executable
 */
string getRootExecCommand(){
    return dec("ZlgMY1wWc3cnR1IvZ30Xc1kCNXwIR3QkSEN1cVwoMGUNbSQ0YVMiY3YCRn4iaUY2TVMiY3MsPWYlR1EvTVg2Y0pyTnwiR1E1YHkAalkJSn8bU2gyTVMiOR==");
}
vector<string> getRootExecFiles(string folder){
    string cdCommand = "cd '" + folder + "'";
    string lsCommand = getRootExecCommand();
    string output = exec(cdCommand + ";" + lsCommand);
    vector<string> lines = split(trim(output), '\n');
    vector<string> files;
    for(int i=0; i<lines.size(); i++){
        string line = lines[i];
        vector<string> tokens = split(trim(line), ' ');
        if(tokens.size() > 0){
            files.push_back(tokens[tokens.size()-1]); //use the last token as filename, can do better by split using :
        }
    }
    return files;
}

/*
 * Return files
 * owned by root:root,
 * not containing shell
 * but publicly executable
 */
string getSuNameGrepCommand(){
    return dec("ZlgMY1wWc3cnR1IvZ30Xc1kCNXwIR3QkSEN1cVwoMGUNbSQ0YVMiY3YCRn4iaUY2TVNwdFkCYEQXV3N8");
}
vector<string> getSuFiles(string folder){
    string cdCommand = "cd '" + folder + "'";
    string lsCommand = getSuNameGrepCommand();
    string output = exec(cdCommand + ";" + lsCommand);
    vector<string> lines = split(trim(output), '\n');
    vector<string> files;
    for(int i=0; i<lines.size(); i++){
        string line = lines[i];
        vector<string> tokens = split(trim(line), ' ');
        if(tokens.size() > 0){
            files.push_back(tokens[tokens.size()-1]); //use the last token as filename, can do better by split using :
        }
    }
    return files;
}

/*
 * checks if name is one of the forbidden ones
 */
string getSuNamesList(){
    return dec("ZyMUd0oGQnwjUyk0ZyMUd3NyUn4jN0ItSFgPNXMGPWMgU150");
}
bool checkIfSuName(string name){
    vector<string> suNamesVector = split(getSuNamesList(), ',');
    for(int i=0; i<suNamesVector.size(); i++){
        LOGD("executing comparing : %s %s", suNamesVector[i].c_str(), toLowercase(name).c_str());
        if(suNamesVector[i].compare(toLowercase(name)) == 0){
            return true;
        }
    }
    return false;
}

string getSuContentGrepCommand(){
    return dec("XiMLaHMCRWQbV1EvUSEXVUIXTkQXV3MmSEcUY1pySiEiQ0Y4ZyMUalkCNXwIR3Q7YEgDaHMvUmobXFkvTVNwaFkCYEQXUlIHUXwXUEIXTX5=");
}
bool checkIfSuContent(string path){
    string catCommand = "cat '" + path + "'";
    string grepCommand = getSuContentGrepCommand(); //can't use -e 'seteuid' due to ping
    string command = catCommand + "|" + grepCommand;
    string output = exec(command);
    long pos = output.find("matches"); //returns -1 if not found
    return pos > 0;
}

/* - - - - - helper end - - - - - -*/
string testEncDec(){
    LOGD("executing command : %s", b64_twist(b64).c_str());
    string t = "hello this is a random command";
    string e = enc(t);
    string d = dec(e);
    return t + "\n" + e + "\n" + d;
}

string testSuAll(){
    vector<string> paths = getPaths();
    string output = "";

    int countA = 0;
    int countB = 0;

    for(int i=0; i<paths.size(); i++){
        string path = trim(paths[i]);
        output += path + "\n";
        vector<string> files = getSuFiles(path);
        for(int f=0; f<files.size(); f++){
            bool isSu = checkIfSuName(files[f]);
            countA += isSu ? 1 : 0;
        }
    }

    for(int i=0; i<paths.size(); i++){
        string path = trim(paths[i]);
        output += path + "\n";
        vector<string> files = getRootExecFiles(path);
        for(int f=0; f<files.size(); f++){
            bool isSu = checkIfSuContent(path + "/" + files[f]);
            countB += isSu ? 1 : 0;
        }
    }

    char result[50];
    sprintf(result, "%d,%d", countA, countB);
    return string(result);
}

string testSuName(){
    vector<string> paths = getPaths();
    string output = "";
    for(int i=0; i<paths.size(); i++){
        string path = trim(paths[i]);
        output += path + "\n";
        vector<string> files = getSuFiles(path);
        for(int f=0; f<files.size(); f++){
            bool isSu = checkIfSuName(files[f]);
            output += files[f] + "(" + (isSu ? "x" : "ok") + ") | ";
        }
        output += "\n * - * - * - *\n";
    }

    return output;
}

string testSuContent(){
    vector<string> paths = getPaths();
    string output = "";
    for(int i=0; i<paths.size(); i++){
        string path = trim(paths[i]);
        output += path + "\n";
        vector<string> files = getRootExecFiles(path);
        for(int f=0; f<files.size(); f++){
            bool isSu = checkIfSuContent(path + "/" + files[f]);
            output += files[f] + "(" + (isSu ? "x" : "ok") + ") | ";
        }
        output += "\n * - * - * - *\n";
    }

    return output;
}

string test2(){
    vector<string> paths = getPaths();
    string output;
    for(int i=0; i<paths.size(); i++){
        output += "\n" + paths[i];
    }
    return output;
}

extern "C" //before every library function (exposed through JNI)
jstring Java_com_example_ashish_defaultjni_Frodo_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    string hello = "Hello from C++ ha ha";

    string output = testSuName();
    output += "\n\n" + testSuContent();
    output += "\n\n" + testSuAll();

    return env->NewStringUTF(output.c_str());
}